import 'package:flutter/material.dart';

import 'searchjobpage.dart';
import 'offerjobpage.dart';
import 'profilepage.dart';

class HomePage extends StatefulWidget {
  final Map profile;
  HomePage({Key key, @required this.profile}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Key keySearchJobPage = PageStorageKey('searchJobPage');
  final Key keyOfferJobPage = PageStorageKey('offerJobPage');
  final Key keyProfilePage = PageStorageKey('profilePage');

  int currentTab = 0;

  SearchJobPage searchJobPage;
  OfferJobPage offerJobPage;
  ProfilePage profilePage;

  List<Widget> pages;
  Widget currentPage;

  List<Data> dataList;

  @override
  void initState() {
    dataList = [
      Data(1, false, "Example-1"),
      Data(2, false, "Example-2"),
      Data(3, false, "Example-3"),
      Data(4, false, "Example-4"),
      Data(5, false, "Example-5"),
    ];
    searchJobPage = SearchJobPage(
      key: keySearchJobPage, 
      dataList: dataList,);
    offerJobPage = OfferJobPage(key: keyOfferJobPage);
    profilePage = ProfilePage(key: keyProfilePage, profile: widget.profile,);
    pages = [searchJobPage, offerJobPage, profilePage];
    currentPage = searchJobPage;
    super.initState();
  }

  void onTabTapped(int index) {
    setState(() {
      currentTab = index;
      currentPage = pages[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        brightness: Brightness.dark,
        textTheme: TextTheme(
          title: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
          )
        ),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Color(0xFF2A2D43),
        leading: Icon(Icons.work),
        title: Text('InstaJob'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.chat), onPressed: () => {},)
        ],
      ),
      body: currentPage,
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Colors.blue,
        onTap: onTabTapped,
        currentIndex: currentTab,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text('Search Job'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.work),
            title: Text('Offer Job'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile')
          )
       ],
     ),
    );
  }
}
