import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  final Map profile;
  ProfilePage({Key key, this.profile}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      color: Color(0xFFFAFAFF),
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              height: 150.0,
              child: Image.network(widget.profile['picture'])
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0)
            ),
            Flexible(
              flex: 3,
              child: ListView(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text(widget.profile['name']),
                  ),
                  ListTile(
                    leading: Icon(Icons.mail),
                    title: Text(widget.profile['email']),
                  ),
                  ListTile(
                    leading: Icon(Icons.phone),
                    title: Text('Phone'),
                  ),
                  ListTile(
                    leading: Icon(Icons.thumb_up),
                    title: Text('Rating'),
                  ),
                  ListTile(
                    leading: Icon(Icons.archive),
                    title: Text('Curriculum Vitae'),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  ButtonTheme(
                    minWidth: 300.0,
                    height: 40.0,
                    child: RaisedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text('Logout'),
                      textColor: Colors.white,
                      color: Color(0xFF7D1128),
                      splashColor: Colors.white,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                    ),
                  ),
                ],
              ),
            )
          ]
        ),
      ),
    );
  }
}
