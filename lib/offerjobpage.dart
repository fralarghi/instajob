import 'package:flutter/material.dart';

class OfferJobPage extends StatefulWidget {
  OfferJobPage({Key key}) : super(key: key);

  @override
  _OfferJobPageState createState() => _OfferJobPageState();
}

class _OfferJobPageState extends State<OfferJobPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Flex(
        direction: Axis.vertical,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: ListView.builder(
              itemExtent: 100.0,
              itemBuilder: (context, index) => Container(
                padding: EdgeInsets.all(10.0),
                child: Material(
                  elevation: 4.0,
                  borderRadius: BorderRadius.circular(5.0),
                  color: index % 2 == 0 ? Colors.grey : Colors.blue,
                  child: Center(child:Text(index.toString())),
                ),
              )
            ),
          )
        ],
      ),
    );
  }
}