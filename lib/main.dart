import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'homepage.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
      home: LoginPage(title: 'Instajob'),
    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin{

  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;
  var _emailController = TextEditingController();
  var _passwordController = TextEditingController();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  bool isLoggedIn = false;
  Map profile = {};

  void onLoginStatusChanged(bool isLoggedIn, Map profile) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
      this.profile = profile;
    });
    if(isLoggedIn){
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => new HomePage(profile: profile)
      );
      Navigator.of(context).push(route);
    }
  }

  Map defaultProfile = {
    'name':'Name Surname', 
    'email':'name.surname@instajob.com', 
    'picture': 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
  };

  login() {
      onLoginStatusChanged(true, defaultProfile);
  }

  void initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult = await facebookLogin.logInWithReadPermissions(['email']);

    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        onLoginStatusChanged(false, {});
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        onLoginStatusChanged(false, {});
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        var facebookGraphResponse = await http.get(
          'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,birthday,hometown,picture&access_token=${facebookLoginResult
          .accessToken.token}');
        profile = json.decode(facebookGraphResponse.body);

        print(profile["id"]);

        var facebookGraphPictureResponse = await http.get(
          'https://graph.facebook.com/${profile['id']}?fields=picture.type(large)&access_token=${facebookLoginResult
          .accessToken.token}');
          
        print(json.decode(facebookGraphPictureResponse.body));
        profile['picture'] = json.decode(facebookGraphPictureResponse.body)['picture']['data']['url'];
        
        onLoginStatusChanged(true, profile);
    }
  }

  @override
  void initState(){
    super.initState();
    _iconAnimationController = new AnimationController(
      vsync: this, duration: new Duration(milliseconds: 3000));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController, curve: Curves.bounceOut);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) { 
    return Scaffold(
      backgroundColor: Color(0xFF2A2D43),
      body: new Center(
        child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Image(
                      image: new AssetImage("assets/instajob_full_tras.png"),
                      fit: BoxFit.cover,
                      height: _iconAnimation.value * 200,
                      color: Colors.white,
                    ),
                    new Form(
                      child: new Theme(
                        data: new ThemeData(
                          brightness: Brightness.dark,
                          primaryColor: Colors.orangeAccent,
                          inputDecorationTheme: new InputDecorationTheme(
                            labelStyle: new TextStyle(
                              color: Colors.orangeAccent, fontSize: 20.0
                            )
                          )
                        ),
                        child: new Container(
                          padding: const EdgeInsets.all(40.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              new TextFormField(
                                controller: _emailController,
                                focusNode: _emailFocus,
                                onFieldSubmitted: (term){
                                  _emailFocus.unfocus();
                                  FocusScope.of(context).requestFocus(_passwordFocus);
                                },
                                decoration: new InputDecoration(
                                  labelText: "Email",
                                  labelStyle: TextStyle(color: Colors.white),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.greenAccent)
                                  )
                                ),
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.next,
                              ),
                              new TextFormField(
                                controller: _passwordController,
                                focusNode: _passwordFocus,
                                onFieldSubmitted: (term){
                                  _passwordFocus.unfocus();
                                },
                                decoration: new InputDecoration(
                                  labelText: "Password",
                                  labelStyle: TextStyle(color: Colors.white),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.greenAccent)
                                  )
                                ),
                                keyboardType: TextInputType.text,
                                obscureText: true,
                                textInputAction: TextInputAction.done,
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Container(
                                alignment: Alignment(1.0, 0.0),
                                padding: EdgeInsets.only(top: 15.0, left: 20.0),
                                child: InkWell(
                                  child: Text('Forgot Password',
                                    style: TextStyle(
                                      color: Color(0xaaFFFFFF),
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline
                                      )
                                  )
                                )
                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              new MaterialButton(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                minWidth: double.infinity,
                                height: 50.0,
                                color: Colors.white,
                                textColor: Colors.black,
                                child: new Text("Login"),
                                onPressed: () => login(),
                                splashColor: Colors.white,
                              ),
                              new Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                              ),
                              new MaterialButton(
                                minWidth: double.infinity,
                                height: 50.0,
                                color: Color(0xFF3B5998),
                                textColor: Colors.white,
                                child: new Text("Continue with Facebook"),
                                onPressed: () => initiateFacebookLogin(),
                                splashColor: Colors.white,
                              ),
                            ],
                          )
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        )
      ),
    );
  }
}

