import 'package:flutter/material.dart';

class SearchJobPage extends StatefulWidget {
  final List<Data> dataList;
  SearchJobPage({Key key, this.dataList}) : super(key: key);
  @override
  _SearchJobPageState createState() => _SearchJobPageState();
}

class _SearchJobPageState extends State<SearchJobPage> {

  double _sliderValue;
  List _jobCategories = ["Waiter", "Baby-sitter", "Barman", "Speedy-pizza", "Dog-sitter"];
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentCategory;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentCategory = _dropDownMenuItems[0].value;
    _sliderValue = 20.0;
    super.initState();
  }

  Widget get insertRadius {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Icon(Icons.my_location),
          ),
          Flexible(
            flex: 1,
            child: Slider(
              activeColor: Color(0xFF6A7FDB),
              min: 0.0,
              max: 100.0,
              onChanged: (newRating) {
                setState(() => _sliderValue = newRating);
              },
              value: _sliderValue,
            ),
          ),
          Container(
            width: 75.0,
            alignment: Alignment.center,
            child: Text('${_sliderValue.toInt()} km',
              style: TextStyle(fontSize: 20)),
          ),
        ],
      ),
    );
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String category in _jobCategories) {
      items.add(new DropdownMenuItem(
        value: category,
        child: Text(category),
      ));
    }
    return items;
  }

    void changedDropDownItem(String selectedCategory) {
    setState(() {
      _currentCategory = selectedCategory;
    });
  }

  Widget get chooseCategory {
    return Container (
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Icon(Icons.work),
          ),
          Flexible( 
            flex: 1,
            child: Center(
              child: DropdownButtonHideUnderline(
                child: ButtonTheme(
                  alignedDropdown: true,
                  child: DropdownButton<String>(
                    value: _currentCategory,
                    items: _dropDownMenuItems,
                    onChanged: changedDropDownItem,
                    iconSize: 40.0,
                  ),
                )
              )
            )
          )
        ],
      )
    ); 
  }

  Widget get searchMatchesButton {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: ButtonTheme(
        minWidth: 300.0,
        height: 40.0,
        child: RaisedButton(
          onPressed: () => print('Matches button pressed!'),
          child: Text('Matches'),
          textColor: Colors.white,
          color: Color(0xFF6A7FDB),
          splashColor: Colors.white,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        ),
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFFAFAFF),
      child: Column(
        children: <Widget>[
          insertRadius,
          Container(height: 1.0,color: Colors.black),
          chooseCategory,
          Container(height: 1.0,color: Colors.black),
          searchMatchesButton,
          Flexible(
            flex: 1,
            child: ListView.builder(
              itemCount: widget.dataList.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  key: PageStorageKey('${widget.dataList[index].id}'),
                  title: Text(widget.dataList[index].title),
                  children: <Widget>[
                    Container(
                      color: index % 2 == 0 ? Colors.orange : Colors.limeAccent,
                      height: 100.0,
                    )
                  ],
                );
              }
            )
          )
        ],
      ),
    );
  }
}

class Data {
  final int id;
  bool expanded;
  final String title;
  Data(this.id, this.expanded, this.title);
}